<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title></title>
    </head>
    <body>
        <p>a : </p>
        <table border="2">
            <thead>
                <tr>
                    <th>Rencontre</th>
                    <th>score</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>France / Angleterre</td>
                    <td>26 - 24</td>
                </tr>
                <tr>
                    <td>Galle / Italie</td>
                    <td>23 - 15</td>
                </tr>
                <tr>
                    <td>Irlande / Ecosse</td>
                    <td>28 - 6</td>
                </tr>
            </tbody>
        </table>
        </br></br>
        <p>b:</p>
        <?php
                $var1 = 'France / Angleterre';
                $var2 = '26 - 24';
                $var3 = 'Galle / Italie';
                $var4 = '23 - 15';
                $var5 = 'Irlande / Ecosse';
                $var6 = '28 - 6';
        ?>
        
        <table border="2">
            <thead>
                <tr>
                    <th>Rencontre</th>
                    <th>score</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><?php echo $var1; ?></td>
                    <td><?php echo $var2; ?></td>
                </tr>
                <tr>
                    <td><?php echo $var3; ?></td>
                    <td><?php echo $var4; ?></td>
                </tr>
                <tr>
                    <td><?php echo $var5; ?></td>
                    <td><?php echo $var6; ?></td>
                </tr>
            </tbody>
        </table>
        </br></br>
        
        <p>c: </p>
        <?php
        $rencontre = array('France / Angleterre', '26 - 24', 'Galle / Italie', '23 - 15', 'Irlande / Ecosse', '28 - 6');
        ?>
        
        <table border="2">
            <thead>
                <tr>
                    <th>Rencontre</th>
                    <th>score</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><?php echo $rencontre[0]; ?></td>
                    <td><?php echo $rencontre[1]; ?></td>
                </tr>
                <tr>
                    <td><?php echo $rencontre[2]; ?></td>
                    <td><?php echo $rencontre[3]; ?></td>
                </tr>
                <tr>
                    <td><?php echo $rencontre[4]; ?></td>
                    <td><?php echo $rencontre[5]; ?></td>
                </tr>
            </tbody>
        </table>
        </br></br>
        
        <p>d: </p>
        <?php
            $rencontres = array('France / Angleterre','26 - 24',
                                'Galles / Italie','23 -15',
                                'Irlande / Ecosse','28 - 6');
        ?>
        <table style="text-align:center" border=2px bordercolor=grey>
            <tr>
                <td>
                    <?php echo $rencontres[0]; ?>
                </td>
                <td>
                    <?php echo $rencontres[1]; ?>
                </td>
            </tr>
             <tr>
                 <td>
                    <?php echo $rencontres[2]; ?>
                 </td>
                 <td>
                    <?php echo $rencontres[3]; ?>
                 </td>
             </tr>
             <tr>
                 <td>
                    <?php echo $rencontres[4]; ?>
                 </td>
                 <td>
                    <?php echo $rencontres[5]; ?>
                 </td>
             </tr>
        </table>
        </br>
        <table style="text-align:center" border=2px bordercolor=grey>
            <?php
            for ($i=0; $i<3; $i++)
            {
            ?>
            <tr>
                <td>
                    <?php echo $rencontres[2*$i]; ?>
                </td>
                <td>
                    <?php echo $rencontres[2*$i+1]; ?>
                </td>
            </tr>
            <?php
            }
            ?>
        </table>
        </br>
        <table style="text-align:center" border=2px bordercolor=grey>
            <?php
            for ($i=0; $i<6; $i++)
            {
                if ($i == 0 || $i==2 || $i==4)
                {
            ?>
            <tr>
                <?php
                }
                ?>
                <td>
                    <?php
                    echo $rencontres[$i];
                    ?>
                </td>
                <?php
                if ($i == 1 || $i==3 || $i==5)
                {
                ?>
            </tr>
            <?php
                }
            }
            ?>
        </table>
      
    </body>
</html>
